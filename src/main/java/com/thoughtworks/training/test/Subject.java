package com.thoughtworks.training.test;

public class Subject {

    /**
     * 两整数相加(环境验证)
     *
     * @param number1 整数1
     * @param number2 整数2
     * @return 输入的整数和
     */
    public static int solveTask0(int number1, int number2) {
        return number1 + number2;
    }
}
