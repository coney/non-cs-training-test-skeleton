package com.thoughtworks.training.test;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SubjectTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    /**
     * task 0: 两整数相加(环境验证)
     * 将参数传入的两个整数相加, 返回相加结果
     * task 0用于验证测试环境可用, 运行该测试用例, 确保用例执行通过. 若不通过则请在答题前解决编译或运行问题
     */
    @Test
    public void task_0() {
        assertThat(Subject.solveTask0(1, 2), is(3));
    }

}