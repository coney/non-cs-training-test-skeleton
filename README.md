非计测试题目-后端
----

## 准备开发环境
### 1. 下载代码库

``` bash
git clone https://gitlab.com/coney/non-cs-training-test-skeleton.git
```

### 2. 导入工程

打开Intellij Idea, 点击Open打开工程:

![open](./assets/open.png)

选择刚刚Clone下来的工程所在目录:

![select](./assets/select.png)

导入Gradle工程, 使用默认选项即可, 注意JDK版本设置为1.8:

![import](./assets/import.png)

接下来Idea将会下载相应的Gradle Wrapper和相关依赖, 待工作进度条完成消失后, 验证工程是否被正确导入

### 3. 验证开发

打开`SubjectTest.java`, 点击`SubjectTest`类左边的运行图标, 执行所有测试用例.

运行结果如下图所示, task_0应该能够通过测试, 其他用例均执行失败.  

![verify](./assets/verify.png)

task_0为我们验证环境使用的测试用例, 我们后续的测试则需要同学们进行相应的编码实现, 并通过测试用例验证实现的正确性.

